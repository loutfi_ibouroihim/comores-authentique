FROM openjdk:8-jdk-alpine
MAINTAINER loutfi003@gmail.com
COPY target/comores-0.0.1-SNAPSHOT.jar comores-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/comores-0.0.1-SNAPSHOT.jar"]
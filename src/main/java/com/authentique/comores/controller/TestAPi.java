package com.authentique.comores.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class TestAPi {

    @GetMapping("/test")
    public String getTest() {
        return "test";
    }
}
